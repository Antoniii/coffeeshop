# Java + Sugar = Coffee

*you can buy coffeescript for developer*

## [Задача на одну чашечку кофия](https://www.notabug.org/Tonypythony/NimPortArch)

![](https://gitlab.com/Antoniii/coffeeshop/-/raw/master/il_fullxfull.531459398_muq0.jpg)

|          Язык        | чило слагаемых  (кубы) |  Время расчёта (ОЗУ) |                  Cумма                |
| :-------------------:|:----------------------:| :------------------: |:-------------------------------------:|
|       Python 3.6     |       600_000_000      |        ~ 10 min      |   32400000108000000090000000000000000 |
|           C          |       6000000000       | ~ 3 min (~0,6% MEM)  | 2112860691442759676780547521101430784 |
|  CoffeeScript (Node) |       6000000000       | ~ 4 min (~2,5% MEM)  |         3.2400000010800585e+38        |

### Sum with formula is: 3.2400000010800003e+38


## Источники

* [Free Node.js Book](https://books.goalkicker.com/NodeJSBook/)
* [CoffeeScript - Syntax - Tutorialspoint](https://www.tutorialspoint.com/coffeescript/coffeescript_syntax.htm)
* [Подборка шпаргалок для программистов](https://tproger.ru/digest/top-cheatsheets/)
* [Smooth-CoffeeScript](https://github.com/autotelicum/Smooth-CoffeeScript/downloads)
* [Ваша первая чашечка CoffeeScript](https://www.ibm.com/developerworks/ru/library/wa-coffee1/)
* [Try Coffeescript](https://coffeescript.org/#try:alert%20'Hello%20CoffeeScript!'%0Aalert%20'Hi)
* [JDoodle - free Online  Compiler, Editor for Java, C/C++, etc](https://www.jdoodle.com/)